#pragma once
class Queue {
private:
	double *arr;
	int size;
	int firstPointer;
	int lastPointer;
	bool flag;
public:
	friend class Iterator;
	Queue(int size);
	Queue(const Queue &copy);
	void addElement(double element);
	double getElem();
	double checkFirstElem();
	void makeEmpty();
	int getSize();
	bool isEmpty();
};